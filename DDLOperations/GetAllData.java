package DDLOperations;

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.SQLException;
	import java.sql.Statement;
	/**
	 * this is the implementation of the class 
	 * GetAlldata in the given condition
	 * @param Employee
	 */

	public class GetAllData {
		/**
		 * this is the helper method to get 
		 * all values in a Table
		 * @param Employee
		 */
		public void GetAllData(String tablename) {
			String jdbcUrl = "jdbc:mysql://localhost/ziya";
			String username = "root";
			String password = "Ziya@0915";
			try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
					Statement stmt = conn.createStatement();) {
				// Get all records by condition
				String sql = "SELECT * FROM " + tablename + " ; ";
				stmt.executeUpdate(sql);
				System.out.println("This are all the records of the given tablename");

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

}
