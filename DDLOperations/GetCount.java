package DDLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * this is the implementation of the class 
 * getcount in the given condition
 * @param Employee
 */

public class GetCount {
	/**
	 * this is the helper method to get 
	 * count in a Table
	 * @param Employee
	 */
	public void GetCount(String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
			// Get count of records by condition
			String sql = "SELECT COUNT * FROM " + tablename + " ; ";
			stmt.executeUpdate(sql);
			System.out.println("This is the count of the records of the given tablename");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
