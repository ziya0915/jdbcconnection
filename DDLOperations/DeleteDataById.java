package DDLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * this is the implementation of the class 
 * DeleteDataById in the given condition
 * @param Employee
 */

public class DeleteDataById {
	/**
	 * this is the helper method to delete values by 
	 * given condition in a Table
	 * @param Employee
	 */
	public void DeleteDataById(int id, String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
			// delete all records by ID
			String sql = "Delete FROM " + tablename + " WHERE EmployeeID = " + id + " ; ";
			System.out.println(sql);
			stmt.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("Record not deleted....");
			e.printStackTrace();
		}
	}
}
