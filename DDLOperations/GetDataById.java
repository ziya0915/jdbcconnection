package DDLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *this is the implementation of class GetDataByID
 *@author Ziya1 
 */
public class GetDataById {

	/**
	 * this is the helper method to get values by 
	 * given condition in a Table
	 * @param Employee
	 */
	public void GetDataById(int id, String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
			// Select all records by ID
			String sql = "SELECT * FROM " + tablename +" WHERE EmployeeID = "+id+" ; ";
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("This is the Data By Given ID");
			 //Display values which has the given condition
			while(rs.next()) {
            System.out.println("ID          : " + rs.getInt("EmployeeID"));
            System.out.println("Name        : " + rs.getString("Name"));
            System.out.println("D_O_B       : " + rs.getString("D_O_B"));
            System.out.println("Gender      : " + rs.getString("Gender"));
            System.out.println("Designation : " + rs.getString("Designation"));
            System.out.println("Domain      : " + rs.getString("Domain"));
			}
		} catch (SQLException e) {
			System.out.println("Result not found for the given condition");
		}
	}
}