package DMLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *this is the implementation of class Insert values
 *@author Ziya1 
 */

public class InsertValues {
	/**
	 * this is the helper method to Insert values in a Table
	 * @param Employee
	 */
	public void Insertvalues(String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {

			String sql = "insert into " + tablename
					+ " values(101,'ziya','15-2-2001','Female','Software engineer','Java'),"
					+ "(102,'sonu','18-3-1996','Male','Software Developer','Devops'),"
					+ "(103,'monu','26-1-1998','Male','Software Testing','QE'),"
					+ "(104,'zara','23-4-2000','Female','Java Developer','Python'),"
					+ "(105,'zain','10-6-2003','Male','Civil engineer','Civil');";
			int rows = stmt.executeUpdate(sql);
			if (rows == 0) {
				System.out.println("Record can not inserted..");
			} else {
				System.out.println("A new record can be inserted Sucessfullyy...");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Records were not inserted");
		}
	}
}