package DMLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *this is the implementation of class CreateTable
 *@author Ziya1 
 */
public class CreateTable {
	
	/*
	 * these are the columns of the created table
	 * Employee 
	 */
 String Query ="CREATE TABLE Employee("
        + "EmployeeID INT ,"
        + "Name VARCHAR(225),"
        + "D_O_B VARCHAR(255),"
        + "Gender VARCHAR(255),"
        + "Designation VARCHAR(225),"
        + "Domain VARCHAR(255),"
        + "PRIMARY KEY (EmployeeID))";
	/**
	 * this is the helper method to Create Table
	 * @param Employee
	 */
	public void CreateTable(String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		try {
			// Open a connection
			Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(Query);
			System.out.println("Table created Sucessfullyy....");
			conn.close();
		} catch (SQLException e) {
			System.out.println("Table already exists...");
		}
	}
}

