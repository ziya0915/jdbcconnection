package DMLOperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *this is the implementation of delete table
 *@author Ziya1 
 */

public class DeleteTable {
	/**
	 * this is the helper method to delete Table
	 * @param Employee
	 */
	public void deleteTable(String tablename) {
		String jdbcUrl = "jdbc:mysql://localhost/ziya";
		String username = "root";
		String password = "Ziya@0915";
		// Open a connection
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
			String sql = "DROP TABLE " + tablename + ";";
			stmt.executeUpdate(sql);
			System.out.println("Table deleted in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
