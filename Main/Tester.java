package Main;

import java.util.Scanner;

import DDLOperations.DeleteDataById;
import DDLOperations.GetAllData;
import DDLOperations.GetCount;
import DDLOperations.GetDataById;
import DMLOperations.CreateTable;
import DMLOperations.DeleteTable;
import DMLOperations.InsertValues;
import DMLOperations.UpdateValues;

public class Tester {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("1. Create Table ");
		System.out.println("2. Insert Values");
		System.out.println("3. Get Data By Id");
		System.out.println("4. Delete data By Id");
		System.out.println("5. Update the values");
		System.out.println("6. Delete table");
		System.out.println("7. Get All Data");
		System.out.println("Enter your choice :");
		int ch = sc.nextInt();

		// using the switch case for choosing the option

		switch (ch) {
		/*
		 * call the method CreateTable for the creation of Table Employee
		 */
		case 1:
			CreateTable create = new CreateTable();
			create.CreateTable("Employee");
			break;

		/*
		 * call the method InsertValues for inserting the values in a Table Employee
		 */
		case 2:
			InsertValues insert = new InsertValues();
			insert.Insertvalues("Employee");
			break;

		/*
		 * call the method GetDataById for getting the values for the given condition in
		 * a Table Employee
		 */
		case 3:
			GetDataById byid = new GetDataById();
			byid.GetDataById(103, "Employee");
			break;

		/*
		 * call the method GetDataById for getting the values for the given condition in
		 * a Table Employee
		 */
		case 4:
			DeleteDataById dbyid = new DeleteDataById();
			dbyid.DeleteDataById(101, "Employee");
			break;

		/*
		 * call the method update values for updating the values for the given condition
		 * in a Table Employee
		 */
		case 5:
			UpdateValues update = new UpdateValues();
			update.UpdatevalueById(103, "Employee");
			break;

		/*
		 * call the method deleteTable for deleting the Table Employee
		 */
		case 6:
			DeleteTable delete = new DeleteTable();
			delete.deleteTable("Employee");
			break;

		/*
		 * call the method getalldata for getting all the values the Table Employee
		 */
		case 7:
			GetAllData getdata = new GetAllData();
			getdata.GetAllData("Employee");
			break;
			
			/*
			 * call the method getcount all the values the Table Employee
			 */
		case 8:
				GetCount count = new GetCount();
				count.GetCount("Employee");
				break;
		default:
			System.out.println("invalid choice....");

		}
	}
}